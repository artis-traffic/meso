/**
 * @file artis-traffic/meso/utils/Counter.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MESO_UTILS_COUNTER_HPP
#define ARTIS_TRAFFIC_MESO_UTILS_COUNTER_HPP

#include <iomanip>
#include <iostream>

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include "../core/Vehicle.hpp"

namespace artis::traffic::meso::utils {

class Counter
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Counter> {
public:
  struct inputs {
    enum values {
      IN
    };
  };

  struct outputs {
    enum values {
      OUT_CONFIRM
    };
  };

  struct vars {
    enum values {
      COUNTER, VEHICLE
    };
  };

  Counter(const std::string &name,
          const artis::pdevs::Context<artis::common::DoubleTime, Counter> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Counter>(name, context) {
    input_port({inputs::IN, "in"});
    observables({{vars::COUNTER, "counter"},
                 {vars::VEHICLE, "vehicle"}
                });
    output_port({outputs::OUT_CONFIRM, "out_confirm"});
  }

  ~Counter() override = default;

  void dint(const artis::traffic::core::Time & /* t */) override {
    if (_phase == Phase::SEND_CONFIRM) {
      _phase = Phase::WAIT;
      _sigma = artis::common::DoubleTime::infinity;
    }
  }

  void dext(const artis::traffic::core::Time &t, const artis::traffic::core::Time &e,
            const artis::traffic::core::Bag &bag) override {
    _counter += bag.size();

    std::for_each(bag.begin(), bag.end(),
                  [e, t, this](const artis::traffic::core::ExternalEvent &event) {
                    core::Vehicle vehicle;

                    event.data()(vehicle);
                    _vehicle = vehicle;
                    _vehicle.data().exit(t);
                    _phase = Phase::SEND_CONFIRM;
                    _sigma = 0;

                  });

//    std::cout << "[" << get_name() << "]: " << _counter << std::endl;

  }

  void start(const artis::traffic::core::Time & /* t */) override {
    _counter = 0;
    _sigma = artis::common::DoubleTime::infinity;
    _phase = Phase::WAIT;
    _vehicle = {};
    _vehicle.data().t_real_end = artis::common::DoubleTime::infinity;
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override {
    return _sigma;
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override {
    artis::traffic::core::Bag bag;
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_CONFIRM, _vehicle));
    return bag;
  }

  artis::common::event::Value
  observe(const artis::traffic::core::Time &t, unsigned int index) const override {
    if (index == vars::COUNTER) {
      return _counter;
    } else if (index == vars::VEHICLE) {
      if (_vehicle.data().t_real_end == t) {
        return artis::common::event::Value(_vehicle.to_json().c_str(), _vehicle.to_json().size());
      }
      return {};
    } else {
      return {};
    }
  }

private:
  struct Phase {
    enum values {
      WAIT, SEND_CONFIRM
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case WAIT:
          return "WAIT";
        case SEND_CONFIRM:
          return "SEND_CONFIRM";
      }
      return "";
    }
  };

  unsigned int _counter;
  artis::traffic::core::Time _sigma;
  Phase::values _phase;
  core::Vehicle _vehicle;
};

}

#endif