/**
 * @file artis-traffic/meso/utils/SimpleGenerator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/meso/core/Vehicle.hpp>

#include <random>
#include <iostream>

#ifndef ARTIS_TRAFFIC_MESO_SIMPLE_GENERATOR_HPP
#define ARTIS_TRAFFIC_MESO_SIMPLE_GENERATOR_HPP

namespace artis::traffic::meso::utils {

struct SimpleGeneratorParameters {
  unsigned int start_index;
  double capacity;
};

class SimpleGenerator
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, SimpleGenerator, SimpleGeneratorParameters> {
public :
  struct inputs {
    enum values {
      IN_OPEN, IN_CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  struct vars {
    enum values {
      COUNTER
    };
  };

  SimpleGenerator(const std::string &name,
                  const artis::pdevs::Context<artis::common::DoubleTime,
                    SimpleGenerator,
                    SimpleGeneratorParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, SimpleGenerator, SimpleGeneratorParameters>(name, context),
    _start_index(context.parameters().start_index),
    _capacity(context.parameters().capacity) {
    input_ports({{inputs::IN_OPEN,  "in_open"},
                 {inputs::IN_CLOSE, "in_close"}});
    output_ports({{outputs::OUT, "out"}});
    observables({{vars::COUNTER, "counter"}});
  }

  ~SimpleGenerator() override = default;

  void dint(const artis::traffic::core::Time &t) override {
    if (_opened) {
      ++_index;
      update_sigma(t);
    }
  }

  void start(const artis::traffic::core::Time & /* t */) override {
    _index = _start_index;
    _opened = false;
    _opening_time = artis::common::DoubleTime::infinity;
    _sigma = artis::common::DoubleTime::infinity;
    _remaining_sigma = artis::common::DoubleTime::infinity;
  }

  void dext(const artis::traffic::core::Time &t, const artis::traffic::core::Time &e,
            const artis::traffic::core::Bag &bag) override {
    if (_remaining_sigma != artis::common::DoubleTime::infinity) {
      _remaining_sigma -= e;
      if (_remaining_sigma < 0) {
        _remaining_sigma = 0;
      }
    }
    std::for_each(bag.begin(), bag.end(),
                  [t, e, this](const artis::traffic::core::ExternalEvent &event) {
                    if (event.on_port(inputs::IN_OPEN)) {
                      _opened = true;
                      if (t == 0) {
                        update_sigma(t);
                      } else {
                        if (_remaining_sigma == artis::common::DoubleTime::infinity) {
                          update_sigma(t);
                        } else {
                          _sigma = _remaining_sigma;
                        }
                        _remaining_sigma = artis::common::DoubleTime::infinity;
                      }

//                      std::cout << t << " OPEN: " << _sigma << std::endl;

                    } else if (event.on_port(inputs::IN_CLOSE)) {
                      event.data()(_opening_time);

                      _opened = false;
                      _remaining_sigma = std::max(_opening_time - t, _sigma);
                      _sigma = artis::common::DoubleTime::infinity;

//                      std::cout << t << " CLOSE: " << _remaining_sigma << std::endl;

                    }
                  });
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override {
    return _sigma;
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
    artis::traffic::core::Bag bag;

    if (t > 0 and _opened) {
      artis::traffic::meso::core::Vehicle vehicle = {_index, 4.5, 3, 0, 10, 1, 1, 1, {0, 0}, {}};

      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
    }
    return bag;
  }

  artis::common::event::Value observe(const artis::traffic::core::Time & /* t */,
                                      unsigned int index) const override {
    switch (index) {
      case vars::COUNTER:
        return (unsigned int) (_index - _start_index);
      default:
        return {};
    }
  }

private:
  void update_sigma(const artis::traffic::core::Time &t) {
    if (_opened) {
      if (t < 1800) {
        _sigma = 1800 - t;
      } else if (t <= 3600) {
        _sigma = 1. / ((_capacity - 0.01) * ((t - 1800) / 1800) + 0.01);
      } else if (t <= 10800) {
        _sigma = 1. / _capacity;
//      } else if (t <= 7200) {
//        _sigma = 1. / ((_capacity - 0.1) * (1 - (t - 5400) / 1800) + 0.1);
      } else {
        _sigma = artis::common::DoubleTime::infinity;
      }
    } else {
      _sigma = artis::common::DoubleTime::infinity;
    }
  }

  // parameters
  unsigned int _start_index;
  double _capacity;

  // state
  unsigned int _index;
  bool _opened;
  artis::traffic::core::Time _sigma;
  artis::traffic::core::Time _opening_time;
  artis::traffic::core::Time _remaining_sigma;
};

}

#endif //ARTIS_TRAFFIC_MESO_SIMPLE_GENERATOR_HPP