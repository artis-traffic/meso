/**
 * @file artis-traffic/meso/utils/End.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/meso/core/Vehicle.hpp>

#include <cmath>

#ifndef ARTIS_TRAFFIC_MESO_END_HPP
#define ARTIS_TRAFFIC_MESO_END_HPP

namespace artis::traffic::meso::utils {

struct EndParameters {
  double capacity;
};

class End
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, End, EndParameters> {

public:
  struct inputs {
    enum values {
      IN
    };
  };

  struct outputs {
    enum values {
      OUT_CONFIRM, OUT_OPEN, OUT_CLOSE
    };
  };

  struct vars {
    enum values {
      VEHICLE_NUMBER, TOTAL_VEHICLE_NUMBER
    };
  };

  End(const std::string &name,
      const artis::pdevs::Context<artis::common::DoubleTime, End, EndParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, End, EndParameters>(name, context),
    _capacity(context.parameters().capacity) {
    input_ports({{inputs::IN, "in"}});
    output_ports({
                   {outputs::OUT_CONFIRM, "out_confirm"},
                   {outputs::OUT_OPEN,    "out_open"},
                   {outputs::OUT_CLOSE,   "out_close"}
                 });
    observables({{vars::VEHICLE_NUMBER,       "vehicle_number"},
                 {vars::TOTAL_VEHICLE_NUMBER, "total_vehicle_number"}});
  }

  ~End() override = default;

  void dconf(
    const artis::traffic::core::Time &t,
    const artis::traffic::core::Time & /* e */,
    const artis::traffic::core::Bag &bag) override {
    dint(t);
    dext(t, 0, bag);
  };

  void dint(const artis::traffic::core::Time &t) override {
    if (_phase == Phase::SEND_OPEN) {
      _phase = Phase::WAIT_AND_OPENED;
      _sigma = artis::common::DoubleTime::infinity;
    } else if (_phase == Phase::SEND_CLOSE) {
      _phase = Phase::WAIT_AND_CLOSED;
      if (t < 3600) {
        _sigma = 1. / _capacity;
      } else if (t < 5400) {
        _sigma = 1. / (_capacity * ((1800 - (t - 3600)) / 1800));
//        _sigma = 1. / (_capacity - 0.01);
      } else {
        _sigma = artis::common::DoubleTime::infinity;
      }
    } else if (_phase == Phase::WAIT_AND_CLOSED) {
      _phase = Phase::SEND_OPEN;
      _sigma = 0;
    }
    _last_time = t;
  };

  void dext(const artis::traffic::core::Time &t,
            const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag &bag) override {
    std::for_each(bag.begin(), bag.end(),
                  [this](const artis::traffic::core::ExternalEvent &event) {
                    if (event.on_port(inputs::IN)) {
                      event.data()(_vehicle);

                      assert(_phase == Phase::WAIT_AND_OPENED);

                      ++_total_vehicle_number;
                      _phase = Phase::SEND_CLOSE;
                      _sigma = 0;
                    }
                  });
    _last_time = t;
  };

  void start(const artis::traffic::core::Time &t) override {
    _sigma = 0;
    _phase = Phase::SEND_OPEN;
    _total_vehicle_number = 0;
    _last_time = t;
  };

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override {
    return _sigma;
  };

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
    artis::traffic::core::Bag bag;

    if (_phase == Phase::SEND_OPEN) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_OPEN, (unsigned int)0));
    } else if (_phase == Phase::SEND_CLOSE) {
//      bag.push_back(
//        artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE, core::Data{0, (double) (t + 1. / _capacity)}));

      if (t < 3600) {
        bag.push_back(
          artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE, core::Data{0, (double) (t + 1. / _capacity)}));
      } else if (t < 5400) {
        bag.push_back(
          artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE,
                                              core::Data{0, (double) (t +
                                                                      1. / (_capacity * ((1800 - (t - 3600)) / 1800)))}));
//        bag.push_back(
//          artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE,
//                                              core::Data{0, (double) (t +
//                                                                      1. / (_capacity - 0.01))}));
      } else {
        bag.push_back(
          artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE,
                                              core::Data{0, (double) (artis::common::DoubleTime::infinity)}));
      }
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_CONFIRM, _vehicle));
    }
    return bag;
  }

  artis::common::event::Value observe(const artis::traffic::core::Time & /*t*/,
                                      unsigned int index) const override {
    switch (index) {
      case vars::TOTAL_VEHICLE_NUMBER:
        return _total_vehicle_number;
      default:
        return artis::common::event::Value();
    }
  };

private:
  struct Phase {
    enum values {
      WAIT_AND_OPENED, WAIT_AND_CLOSED, SEND_OPEN, SEND_CLOSE
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case WAIT_AND_OPENED:
          return "WAIT_AND_OPENED";
        case WAIT_AND_CLOSED:
          return "WAIT_AND_CLOSED";
        case SEND_OPEN:
          return "SEND_OPEN";
        case SEND_CLOSE:
          return "SEND_CLOSE";
      }
      return "";
    }
  };

  // parameters
  double _capacity;

  // state
  artis::traffic::meso::core::Vehicle _vehicle;
  artis::traffic::core::Time _last_time;
  artis::traffic::core::Time _sigma;
  Phase::values _phase;
  unsigned int _total_vehicle_number;
};

}

#endif //ARTIS_TRAFFIC_MESO_END_HPP