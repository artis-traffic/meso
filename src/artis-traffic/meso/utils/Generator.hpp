/**
 * @file artis-traffic/meso/utils/Generator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MESO_UTILS_GENERATOR_HPP
#define ARTIS_TRAFFIC_MESO_UTILS_GENERATOR_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include "../core/Vehicle.hpp"

#include <random>

namespace artis::traffic::meso::utils {

struct GeneratorParameters {
  unsigned int start_index;
  double min;
  double max;
  double mean;
  double stddev;
  unsigned long seed;
  std::vector<std::vector<unsigned int> > paths;
};

class Generator
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Generator, GeneratorParameters> {
public:
  struct inputs {
    enum values {
      IN_OPEN, IN_CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  struct vars {
    enum values {
      COUNTER
    };
  };

  Generator(const std::string &name,
            const artis::pdevs::Context<artis::common::DoubleTime,
              Generator,
              GeneratorParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Generator, GeneratorParameters>(
      name,
      context),
    _start_index(context.parameters().start_index),
    _min(context.parameters().min),
    _max(context.parameters().max),
    _paths(context.parameters().paths),
    _sigma_distribution(context.parameters().mean, context.parameters().stddev),
    _path_distribution(0, context.parameters().paths.size() - 1) {
    _generator.seed(context.parameters().seed);

    input_ports({{inputs::IN_OPEN,  "in_open"},
                 {inputs::IN_CLOSE, "in_close"}});
    output_ports({{outputs::OUT, "out"}});
    observables({{vars::COUNTER, "counter"}});
  }

  ~Generator() override = default;

  void dint(const artis::traffic::core::Time &t) override {

//    std::cout << t << " [" << get_full_name() << "] =====> dint" << std::endl;

    ++_index;
    if (_paths.size() > 1) {
      _path_index = _path_distribution(_generator);
    } else {
      _path_index = 0;
    }
    _last_time = t;
    _sigma = _sigma_distribution(_generator);
    _sigma = _sigma <= _min ? _min : _sigma;
    _sigma = _sigma >= _max ? _max : _sigma;
//    _sigma = artis::common::DoubleTime::infinity;
    _supposed_time_to_send = t + _sigma;
  }

  void start(const artis::traffic::core::Time &t) override {

//    std::cout << t << " [" << get_full_name() << "] =====> start" << std::endl;

    _index = _start_index;
    if (_paths.size() > 1) {
      _path_index = _path_distribution(_generator);
    } else {
      _path_index = 0;
    }
    _last_time = t;
    _sigma = _sigma_distribution(_generator);
    _sigma = _sigma <= _min ? _min : _sigma;
    _sigma = _sigma >= _max ? _max : _sigma;
//    _sigma = 1;
    _supposed_time_to_send = t + _sigma;
  }

  void dext(const artis::traffic::core::Time &t, const artis::traffic::core::Time &e,
            const artis::traffic::core::Bag &bag) override {

    std::for_each(bag.begin(), bag.end(),
                  [e, t, this](const artis::traffic::core::ExternalEvent &event) {
                    if (event.on_port(inputs::IN_OPEN)) {
                      _sigma = std::max(0., _supposed_time_to_send - t);
                    } else if (event.on_port(inputs::IN_CLOSE)) {
                      _sigma = artis::common::DoubleTime::infinity;
                    }
                  });
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override {
    return _sigma;
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
    artis::traffic::core::Bag bag;

    if (t > 0) {
      artis::traffic::meso::core::Vehicle vehicle = {_index, 4.5, 3, 0, 10, 1, 1, 1, _paths.at(_path_index), {}};

#ifdef WITH_TRACE
      common::Trace<common::DoubleTime>::trace()
          << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                      common::FormalismType::PDEVS,
                                                      common::FunctionType::LAMBDA,
                                                      common::LevelType::USER)
          << "vehicle = " << vehicle.to_string();
      common::Trace<common::DoubleTime>::trace().flush();
#endif

//      std::cout << t << " [" << get_full_name() << "] =====> lambda" << std::endl;

      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
    }
    return bag;
  }

  artis::common::event::Value observe(const artis::traffic::core::Time & /* t */,
                                      unsigned int index) const override {
    switch (index) {
      case vars::COUNTER:
        return (unsigned int) (_index - _start_index);
      default:
        return artis::common::event::Value();
    }
  }

  common::DoubleTime::type
  lookahead(const common::DoubleTime::type & /* t */) const override { return _last_time + _sigma; }

private:
  // parameters
  unsigned int _start_index;
  double _min;
  double _max;
  std::vector<std::vector<unsigned int> > _paths;

  // state
  unsigned int _index;
  artis::traffic::core::Time _supposed_time_to_send;
  artis::traffic::core::Time _last_time;
  artis::traffic::core::Time _sigma;
  std::default_random_engine _generator;
  std::normal_distribution<double> _sigma_distribution;
  std::uniform_int_distribution<size_t> _path_distribution;
  size_t _path_index;
};

}

#endif