/**
 * @file artis-traffic/meso/utils/JsonReader.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MESO_JSON_READER_HPP
#define ARTIS_TRAFFIC_MESO_JSON_READER_HPP

#include <artis-traffic/utils/JsonReader.hpp>

namespace artis::traffic::meso::utils {

struct InputData {
  unsigned int start_index;
  double min;
  double max;
  double mean;
  double stddev;
  unsigned long seed;
  std::vector<std::vector<unsigned int> > paths;

  void parse(const nlohmann::json &data) {
    start_index = data["start index"].get<unsigned int>();
    min = data["min"].get<double>();
    max = data["max"].get<double>();
    mean = data["mean"].get<double>();
    stddev = data["stddev"].get<double>();
    seed = data["seed"].get<unsigned long>();
    for (const json &path: data["paths"]) {
      paths.emplace_back();
      for (const json &choice: path) {
        paths.back().push_back(choice.get<unsigned int>());
      }
    }
  }
};

struct OutputData {
  enum Type {
    COUNTER, END
  };

  Type type;

  void parse(const nlohmann::json &data) {
    type = data["type"].get<std::string>() == "end" ? END : COUNTER;
  }
};

struct NodeData {
  double transit_duration;
  std::vector<double> proportions;

  void parse(const nlohmann::json &data) {
    transit_duration = data["transit duration"].get<double>();
    for (const json &proposition: data["proportions"]) {
      proportions.push_back(proposition.get<double>());
    }
  }
};

struct LinkData {
  double concentration;
  double wave_speed;
  double capacity;

  void parse(const nlohmann::json &data) {
    concentration = data["concentration"].get<double>();
    wave_speed = data["wave speed"].get<double>();
    capacity = data["capacity"].get<double>();
  }
};

typedef artis::traffic::utils::Network<InputData, OutputData, NodeData, LinkData> Network;
typedef artis::traffic::utils::JsonReader<InputData, OutputData, NodeData, LinkData> JsonReader;

}

#endif //ARTIS_TRAFFIC_MESO_JSON_READER_HPP
