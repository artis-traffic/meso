/**
 * @file artis-traffic/meso/core/Node.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "Node.hpp"

namespace artis::traffic::meso::core {

void Node::dconf(const artis::traffic::core::Time &t,
                 const artis::traffic::core::Time &e,
                 const artis::traffic::core::Bag &bag) {

//  std::cout << t << " [" << get_full_name() << "] =====> dconf [before] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

  dint(t);
  dext(t, e, bag);

//  std::cout << t << " [" << get_full_name() << "] =====> dconf [after] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

}

void Node::dint(const artis::traffic::core::Time &t) {

//  std::cout << t << " [" << get_full_name() << "] =====> dint [before] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

  if (_phase == Phase::TRANSIT) {
    _phase = Phase::SEND;
    _sigma = 0;
  } else if (_phase == Phase::SEND) {
    _arrived = false;
    if (not _jam) {
      for (auto &n: _vehicle_numbers) {
        n = 0;
      }
    }
    _vehicles.clear();
    _phase = Phase::WAIT;
    _sigma = artis::common::DoubleTime::infinity;
  } else if (_phase == Phase::SEND_CLOSE) {
    if (_stored_phase == Phase::TRANSIT and _out_index == _index) {
      _phase = Phase::SEND_BACK;
      _sigma = 0;
      _back_vehicles.emplace_back(_vehicle_from, _vehicle);
      _vehicles.clear();
      _arrived = false;
    } else {
      _phase = _stored_phase;
      _sigma = _stored_sigma;
      _open_received.clear();
    }
  } else if (_phase == Phase::SEND_OPEN) {
    _phase = _stored_phase;
    _sigma = _stored_sigma;
    _open_received.clear();
  } else if (_phase == Phase::SEND_BACK) {
    _back_vehicles.clear();
    if (_arrived) {
      _phase = Phase::TRANSIT;
      _sigma = _transit_duration;
    } else {
      _phase = Phase::WAIT;
      _sigma = artis::common::DoubleTime::infinity;
    }
    _stored_phase = _phase;
    _stored_sigma = _sigma;
  }
  _last_time = t;

//  std::cout << t << " [" << get_full_name() << "] =====> dint [after] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

}

void Node::dext(const artis::traffic::core::Time &t,
                const artis::traffic::core::Time &e,
                const artis::traffic::core::Bag &bag) {

//  std::cout << t << " [" << get_full_name() << "] =====> dext [before] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

  _stored_phase = _phase;
  if (_sigma > 0 and _sigma < artis::common::DoubleTime::infinity) {
    _stored_sigma = _sigma - e;
  } else {
    _stored_sigma = _sigma;
  }

  {
    std::for_each(bag.begin(), bag.end(),
                  [e, t, this](const artis::traffic::core::ExternalEvent &event) {
                    if (event.port_index() >= inputs::IN
                        and event.port_index() < inputs::IN_OPEN) {
                      unsigned int index = event.port_index() - inputs::IN;
                      Vehicle vehicle;

                      event.data()(vehicle);
                      _vehicles.emplace_back(index, vehicle);

//                      if (get_name() == "node_309") {
//                        std::cout << "From Node : " << get_name() << " IN at " << t << " " << vehicle.index() << std::endl;
//                      }

//                      std::cout << t << " [" << get_full_name() << "] =====> RECEIVE VEHICLE {"
//                                << vehicle.to_string() << " / " << index << "}" << std::endl;

                      assert(index < _in_number);

                    }
                  });
    if (not _vehicles.empty()) {
      if (_vehicles.size() == 1) {
        _vehicle = _vehicles.back().second;
        _vehicle_from = _vehicles.back().first;
        _vehicle.data().exit(t);
        _phase = Phase::TRANSIT;
        _sigma = _transit_duration;
        _jam = false;
      } else if (_vehicles.size() > 1) {
        _jam = true;
        double min_distance = 0;
        size_t min_index = 0;
        size_t index = 0;
        double sum = std::accumulate(_vehicle_numbers.cbegin(), _vehicle_numbers.cend(), 0.);
        double sumProp = std::accumulate(_proportions.cbegin(), _proportions.cend(), 0.);

        while (index < _vehicle_numbers.size()) {
          double distance = _vehicle_numbers[index] / sum - _proportions[index] / sumProp;

          if (distance < 0 and min_distance > distance) {
            min_index = index;
            min_distance = distance;
          }
          ++index;
        }
        _vehicle = _vehicles.at(min_index).second;
        _vehicle.data().exit(t);
        _vehicle_from = _vehicles.at(min_index).first;
        _vehicles.erase(_vehicles.cbegin() + min_index);
        _vehicle_numbers[min_index]++;
        _back_vehicles = _vehicles;

//        std::cout << t << " [" << get_full_name() << "] =====> SELECT: " << min_index << " [ ";
//        for (const auto& p: _vehicle_numbers) {
//          std::cout << p << " ";
//        }
//        std::cout << "]" << std::endl;

        _phase = Phase::SEND_BACK;
        _sigma = 0;
      }
      _arrived = true;

      assert(_vehicle.current_index() < _out_number);

      _out_index = _vehicle.current_index();
      _stored_phase = _phase;
      _stored_sigma = _sigma;
    }
  }
  std::for_each(bag.begin(), bag.end(),
                [e, t, this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.port_index() >= inputs::IN_OPEN and event.port_index() < inputs::IN_CLOSE) {
                    _index = event.port_index() - inputs::IN_OPEN;

//                    std::cout << t << " [" << get_full_name() << "] =====> OPEN: " << _index
//                              << std::endl;

//                    if (get_name() == "node_309") {
//                      std::cout << "From Node : " << get_name() << " IN OPEN at " << t << " " << _index << std::endl;
//                    }

                    _open_links[_index] = artis::common::DoubleTime::infinity;
                    _open_received.push_back(_index);
                    _phase = Phase::SEND_OPEN;
                    _sigma = 0;
                  } else if (event.port_index() >= inputs::IN_CLOSE) {
                    double open_time;

                    event.data()(open_time);
                    _index = event.port_index() - inputs::IN_CLOSE;

//                    if (get_name() == "node_309") {
//                      std::cout << "From Node : " << get_name() << " IN CLOSE at " << t << " " << _index << std::endl;
//                    }

                    //                    std::cout << t << " [" << get_full_name() << "] =====> CLOSE: " << _index
//                              << std::endl;

                    _open_links[_index] = open_time;
                    _phase = Phase::SEND_CLOSE;
                    _sigma = 0;
                  }
                }
  );
  _last_time = t;

//  std::cout << t << " [" << get_full_name() << "] =====> dext [after] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

}

void Node::start(const artis::traffic::core::Time &t) {
//  std::cout << t << " [" << get_full_name() << "] =====> START" << std::endl;

  _arrived = false;
  _phase = Phase::WAIT;
  _sigma = artis::common::DoubleTime::infinity;
  _last_time = t;
  _jam = false;
}

artis::traffic::core::Time Node::ta(const artis::traffic::core::Time & /* t */) const {
  return _sigma;
}

artis::traffic::core::Bag Node::lambda(const artis::traffic::core::Time & t) const {
  artis::traffic::core::Bag bag;

//  std::cout << t << " [" << get_full_name() << "] =====> lambda [before] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

  if (_phase == Phase::SEND) {
    if (_out_number == 1) {

//      std::cout << t << " [" << get_full_name() << "] =====> SEND VEHICLE {" << _vehicle.to_string()
//                << " / " << _out_index << "}" << std::endl;

//      if (get_name() == "node_309") {
//        std::cout << "From Node : " << get_name() << " SEND at " << t << " " << _vehicle.index() << std::endl;
//      }

      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, _vehicle));
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_CONFIRM + _vehicle_from, _vehicle));
    } else {

//      std::cout << t << " [" << get_full_name() << "] =====> SEND VEHICLE {" << _vehicle.to_string()
//                << " / " << _out_index << "}" << std::endl;

//      if (get_name() == "node_309") {
//        std::cout << "From Node : " << get_name() << " SEND at " << t << " " << _vehicle.index() << std::endl;
//      }

      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT + _out_index, _vehicle));
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_CONFIRM + _vehicle_from, _vehicle));
    }

  } else if (_phase == Phase::SEND_CLOSE) {

//    std::cout << t << " [" << get_full_name() << "] =====> SEND CLOSE {" << _index
//              << "}" << std::endl;

//    if (get_name() == "node_309") {
//      std::cout << "From Node : " << get_name() << " CLOSE at " << t << std::endl;
//    }

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE,
                                                      Data{(unsigned int) _index, _open_links[_index]}));
  } else if (_phase == Phase::SEND_OPEN) {

//    std::cout << t << " [" << get_full_name() << "] =====> SEND OPEN {" << _index
//              << "}" << std::endl;

    for (const auto &r: _open_received) {

//      if (get_name() == "node_309") {
//        std::cout << "From Node : " << get_name() << " OPEN at " << t << " / " << r << std::endl;
//      }

      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_OPEN, (unsigned int) r));
    }
  } else if (_phase == Phase::SEND_BACK) {
    for (const auto &v: _back_vehicles) {

//      std::cout << t << " [" << get_full_name() << "] =====> SEND BACK VEHICLE {" << v.second.index
//                << "} to " << v.first << std::endl;

//      if (get_name() == "node_309") {
//        std::cout << "From Node : " << get_name() << " BACK at " << t << " / " << v.second.index() << std::endl;
//      }

      bag.push_back(artis::traffic::core::ExternalEvent(outputs::BACK + v.first, v.second));
    }
  }

//  std::cout << t << " [" << get_full_name() << "] =====> lambda [after] : " << Phase::to_string(_phase)
//            << " " << _sigma << " " << _arrived << std::endl;

  return bag;
}

artis::common::event::Value Node::observe(const artis::traffic::core::Time &t,
                                          unsigned int index) const {
  switch (index) {
    case vars::ARRIVED:
      return (unsigned int) (_arrived + _back_vehicles.size());
    case vars::VEHICLE:
      if (_vehicle.data().t_real_end == t - _transit_duration) {
        return artis::common::event::Value(_vehicle.to_json().c_str(), _vehicle.to_json().size());
      }
      return {};
    default:
      return {};
  }
}

}