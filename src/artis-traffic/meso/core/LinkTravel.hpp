/**
 * @file artis-traffic/meso/core/LinkTravel.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MESO_CORE_LINKTRAVEL_HPP
#define ARTIS_TRAFFIC_MESO_CORE_LINKTRAVEL_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/meso/core/Vehicle.hpp>

namespace artis::traffic::meso::core {

struct LinkTravelParameters {
  double length;
  unsigned int lane_number;
  double free_speed;
  double concentration;
  double wave_speed;
  double capacity;
  std::deque<std::pair<double, double>> modified_capacities;
  int id_link;
};

class LinkTravel
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, LinkTravel, LinkTravelParameters> {
public:
  struct inputs {
    enum values {
      IN, IN_CONFIRM_OUT
    };
  };

  struct outputs {
    enum values {
      OUT, OUT_OPEN, OUT_CLOSE
    };
  };

  struct vars {
    enum values {
      VEHICLE_NUMBER, TOTAL_VEHICLE_NUMBER, FLOW, SPEED_AVERAGE, MOVING_VEHICLE_NUMBER
    };
  };

  LinkTravel(const std::string &name,
             const artis::pdevs::Context<artis::common::DoubleTime, LinkTravel, LinkTravelParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, LinkTravel, LinkTravelParameters>(name, context),
    _length(context.parameters().length),
    _lane_number(context.parameters().lane_number),
    _free_speed(context.parameters().free_speed),
    _concentration(context.parameters().concentration),
    _wave_speed(context.parameters().wave_speed),
    _capacity(context.parameters().capacity),
    _modified_capacities(context.parameters().modified_capacities),
    _id_link(context.parameters().id_link) {
    input_ports({{inputs::IN,             "in"},
                 {inputs::IN_CONFIRM_OUT, "in_confirm_out"}});
    output_ports({
                   {outputs::OUT,       "out"},
                   {outputs::OUT_OPEN,  "out_open"},
                   {outputs::OUT_CLOSE, "out_close"}
                 });
    observables({{vars::VEHICLE_NUMBER,        "vehicle_number"},
                 {vars::TOTAL_VEHICLE_NUMBER,  "total_vehicle_number"},
                 {vars::FLOW,                  "flow"},
                 {vars::SPEED_AVERAGE,         "speed_average"},
                 {vars::MOVING_VEHICLE_NUMBER, "moving_vehicle_number"}});
  }

  ~LinkTravel() override = default;

  void dconf(
    const artis::traffic::core::Time & /* t* */,
    const artis::traffic::core::Time & /* e */,
    const artis::traffic::core::Bag & /* bag */) override;

  void dint(const artis::traffic::core::Time & /* t */) override;

  void dext(
    const artis::traffic::core::Time & /* t */,
    const artis::traffic::core::Time & /* e */,
    const artis::traffic::core::Bag & /* bag*/) override;

  void start(const artis::traffic::core::Time & /* t */) override;

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                      unsigned int index) const override;

  common::DoubleTime::type
  lookahead(const common::DoubleTime::type & /* t */) const override {
    return _last_time + std::min(_sigma, _open_sigma);
  }

private:
  void update_vehicle_state(const artis::traffic::core::Time &t);

  void update_open_close_state(const artis::traffic::core::Time &t);

  void update_sigma(const artis::traffic::core::Time &t);

  void update_capacity(const artis::traffic::core::Time &t);

  bool vehicle_ready(const artis::traffic::core::Time &t) const;

  struct Entry {
    Vehicle vehicle;
    artis::traffic::core::Time in_time;
    artis::traffic::core::Time next_time;
  };

  struct OpenClosePhase {
    enum values {
      OPENED, SEND_OPEN, SEND_CLOSE, CLOSED
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case OPENED:
          return "OPENED";
        case SEND_OPEN:
          return "SEND_OPEN";
        case SEND_CLOSE:
          return "SEND_CLOSE";
        case CLOSED:
          return "CLOSED";
      }
      return "";
    }
  };

  struct Phase {
    enum values {
      WAIT, SEND
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case WAIT:
          return "WAIT";
        case SEND:
          return "SEND";
      }
      return "";
    }
  };

  // parameters
  double _length;
  unsigned int _lane_number;
  double _free_speed;
  double _concentration;
  double _wave_speed;
  double _capacity;
  std::deque<std::pair<double, double>> _modified_capacities;
  int _id_link;

  // state
  std::deque<Entry> _vehicles;
  unsigned int _total_vehicle_number;
  unsigned int _vehicle_number;
  unsigned int _moving_vehicle_number;
  std::deque<double> _vehicle_numbers;
  std::deque<std::pair<double, double>> _vehicle_durations;
  std::deque<std::pair<unsigned int, artis::traffic::core::Time>> _delays;
  artis::traffic::core::Time _sigma;
  artis::traffic::core::Time _open_sigma;
  artis::traffic::core::Time _modify_sigma;
  OpenClosePhase::values _open_close_phase;
  Phase::values _phase;
  artis::traffic::core::Time _last_time;
};

}

#endif