/**
 * @file artis-traffic/meso/core/Vehicle.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MESO_CORE_VEHICLE_HPP
#define ARTIS_TRAFFIC_MESO_CORE_VEHICLE_HPP

#include <artis-traffic/core/Vehicle.hpp>

namespace artis::traffic::meso::core {

struct VehicleData {
  int link;
  unsigned int number_on_link;
  artis::traffic::core::Time t_entry;
  artis::traffic::core::Time t_estimated_end;
  artis::traffic::core::Time t_real_end;

  void enter(const artis::traffic::core::Time &time, int id_link, unsigned int number,
             const artis::traffic::core::Time &out_time) {
    link = id_link;
    number_on_link = number;
    t_entry = time;
    t_estimated_end = out_time;
    t_real_end = artis::common::DoubleTime::infinity;
  }

  void exit(const artis::traffic::core::Time &time) {
    t_real_end = time;
  }

  bool operator==(const VehicleData &other) const {
    return link == other.link and number_on_link == other.number_on_link and t_entry == other.t_entry and
           t_estimated_end == other.t_estimated_end and t_real_end == other.t_real_end;
  }

  std::string to_json() const {
    return "{ \"link\": " + std::to_string(link) + ","
           + "\"t_entry\": " + std::to_string(t_entry) + ","
           + "\"t_estimated_end\": " + std::to_string(t_estimated_end) + ","
           + "\"t_real_end\": " +
           (t_real_end == artis::common::DoubleTime::infinity ? "\"infinity\"" : std::to_string(t_real_end)) + " }";
  }

  std::string to_string() const {
    return std::to_string(link) + " ; " + std::to_string(number_on_link) + " ; " + std::to_string(t_entry) + " ; " +
           std::to_string(t_estimated_end) + " ; " +
           (t_real_end == artis::common::DoubleTime::infinity ? "infinity" : std::to_string(t_real_end));
  }
};

typedef artis::traffic::core::Vehicle<VehicleData> Vehicle;

struct Data {
  unsigned int index;
  double open_time;

  bool operator==(const Data &other) const {
    return index == other.index and open_time == other.open_time;
  }

  std::string to_string() const {
    return "Data<" + std::to_string(index) + " ; " + std::to_string(open_time) + ">";
  }
};

}

#endif