/**
 * @file artis-traffic/meso/core/Link.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MESO_CORE_LINK_HPP
#define ARTIS_TRAFFIC_MESO_CORE_LINK_HPP

#include <artis-star/kernel/pdevs/GraphManager.hpp>

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/meso/core/Vehicle.hpp>
#include <artis-traffic/meso/core/LinkQueue.hpp>
#include <artis-traffic/meso/core/LinkTravel.hpp>

namespace artis::traffic::meso::core {

struct LinkParameters
{
  double length;
  unsigned int lane_number;
  double free_speed;
  double concentration;
  double wave_speed;
  double capacity;
  unsigned int out_link_number;
  int id_link;
};

class LinkGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime,
    LinkParameters>
{
public:
  enum sub_models
  {
    TRAVEL,
    QUEUE
  };

  struct inputs
  {
    enum values
    {
      IN, IN_CONFIRM_OUT, IN_OPEN, IN_CLOSE, BACK
    };
  };

  struct outputs
  {
    enum values
    {
      OUT, OUT_OPEN, OUT_CLOSE
    };
  };

  LinkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                   const LinkParameters &parameters,
                   const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime,
      LinkParameters>(
      coordinator, parameters, graph_parameters),
    _queue("queue", {parameters.length,
                     parameters.lane_number,
                     parameters.free_speed,
                     parameters.concentration,
                     parameters.wave_speed,
                     parameters.capacity,
                     parameters.out_link_number,
                     parameters.id_link}),
    _travel("travel", {parameters.length,
                       parameters.lane_number,
                       parameters.free_speed,
                       parameters.concentration,
                       parameters.wave_speed,
                       parameters.capacity,
                       {},
                       parameters.id_link})
  {
    add_child(QUEUE, &_queue);
    add_child(TRAVEL, &_travel);

    coordinator->input_ports({{inputs::IN, "in"},
                              {inputs::IN_CONFIRM_OUT, "in_confirm_out"},
                              {inputs::IN_OPEN, "in_open"},
                              {inputs::IN_CLOSE, "in_close"},
                              {inputs::BACK, "back"}});
    coordinator->output_ports({
                                {outputs::OUT, "out"},
                                {outputs::OUT_OPEN, "out_open"},
                                {outputs::OUT_CLOSE, "out_close"}});

    in({coordinator, inputs::IN})
      >> in({&_travel, artis::traffic::meso::core::LinkTravel::inputs::IN});
    in({coordinator, inputs::IN_CONFIRM_OUT})
      >> in({&_travel, artis::traffic::meso::core::LinkTravel::inputs::IN_CONFIRM_OUT});
    in({coordinator, inputs::IN_OPEN})
      >> in({&_queue, artis::traffic::meso::core::LinkQueue::inputs::IN_OPEN});
    in({coordinator, inputs::IN_CLOSE})
      >> in({&_queue, artis::traffic::meso::core::LinkQueue::inputs::IN_CLOSE});
    in({coordinator, inputs::BACK})
      >> in({&_queue, artis::traffic::meso::core::LinkQueue::inputs::BACK});
    out({&_travel, artis::traffic::meso::core::LinkTravel::outputs::OUT})
      >> in({&_queue, artis::traffic::meso::core::LinkQueue::inputs::IN});
    out({&_travel, artis::traffic::meso::core::LinkTravel::outputs::OUT_OPEN})
      >> out({coordinator, outputs::OUT_OPEN});
    out({&_travel, artis::traffic::meso::core::LinkTravel::outputs::OUT_CLOSE})
      >> out({coordinator, outputs::OUT_CLOSE});
    out({&_queue, artis::traffic::meso::core::LinkQueue::outputs::OUT})
      >> out({coordinator, outputs::OUT});
  }

  ~LinkGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkQueue,
    artis::traffic::meso::core::LinkQueueParameters> _queue;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkTravel,
    artis::traffic::meso::core::LinkTravelParameters> _travel;
};

}

#endif