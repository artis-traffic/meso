/**
 * @file artis-traffic/meso/core/LinkQueue.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LinkQueue.hpp"

#include <cmath>
#include <iostream>
#include <iomanip>

namespace artis::traffic::meso::core {

void LinkQueue::dconf(const artis::traffic::core::Time &t,
                      const artis::traffic::core::Time &/* e */,
                      const artis::traffic::core::Bag &bag) {
  dint(t);
  dext(t, 0, bag);
}

void LinkQueue::dint(const artis::traffic::core::Time &t) {

//  if (_id_link == 152) {
//    std::cout << std::setprecision(30) << "From LinkQueue : " << this->_id_link << " dint at " << t << std::endl;
//    std::cout << "[[BEFORE]]" << std::endl;
//    std::cout << std::setprecision(30) << " -> sigma = " << _sigma << std::endl;
//    std::cout << " -> phase = " << Phase::to_string(_phase) << std::endl;
//    std::cout << std::setprecision(30) << " -> delta = " << (_vehicles.front().next_time - t) << std::endl;
//    std::cout << " -> open_links empty = " << _open_links.empty() << std::endl;
//    if (not _open_links.empty()) {
//      std::cout << " -> " << _open_links[_vehicles.front().vehicle.next_index()] << std::endl;
//    }
//  }

  update_state(t);
  _last_time = t;

//  if (_id_link == 152) {
//    std::cout << "[[AFTER]]" << std::endl;
//    std::cout << std::setprecision(30) << " -> sigma = " << _sigma << std::endl;
//    std::cout << " -> phase = " << Phase::to_string(_phase) << std::endl;
//  }

}

void LinkQueue::dext(const artis::traffic::core::Time &t,
                     const artis::traffic::core::Time &e,
                     const artis::traffic::core::Bag &bag) {

//  if (_id_link == 152 or _id_link == 3) {
//    std::cout << "From LinkQueue : " << this->_id_link << " dext at " << t << " " << bag.size() << " "
//              << _vehicles.size() << std::endl;
//  }

  if (_sigma != artis::common::DoubleTime::infinity) {
    _sigma -= e;
  }
  std::for_each(bag.begin(), bag.end(),
                [t, this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {
                    Vehicle vehicle;

                    event.data()(vehicle);

//                    std::cout << t << " => IN QUEUE" << std::endl;

                    if (_vehicles.empty()) {
                      _vehicles.push_back(Entry{vehicle, t, t});
                    } else if (not _vehicles.empty()) {
                      double delay = 1. / (_concentration * _wave_speed) - (1. / _concentration) / _free_speed;

//                      std::cout << "     ==> " << _vehicles.back().next_time + delay << std::endl;

                      _vehicles.push_back(Entry{vehicle, t, _vehicles.back().next_time + delay});
                    }

//                    if (_id_link == 152 or _id_link == 3) {
//                      std::cout << "From LinkQueue : " << this->_id_link << " IN at " << t << " " << vehicle.index() << std::endl;
//                      std::cout << "From LinkQueue : " << this->_id_link << " => " << _vehicles.size() << " / "
//                                << _vehicles.front().next_time << std::endl;
//                      std::cout << "From LinkQueue : " << this->_id_link << " => " << (_vehicles.front().next_time == t) << std::endl;
//                    }

                    update_sigma(t);
                  } else if (event.on_port(inputs::IN_OPEN)) {
                    unsigned int index;

                    event.data()(index);

//                    if (_id_link == 152 or _id_link == 3) {
//                      std::cout << "From LinkQueue : " << this->_id_link << " at " << t << " => OPEN - " << index
//                                << std::endl;
//                    }

                    _open_links[index] = artis::common::DoubleTime::infinity;
                    if (not _vehicles.empty()) {
                      Vehicle &vehicle = _vehicles.front().vehicle;

                      if (vehicle.path().has_next()) {
                        unsigned int out_index = vehicle.next_index();

                        if (_fully_close_links[out_index] && t > _vehicles.front().next_time) {
                          artis::traffic::core::Time delay = t - _vehicles.front().next_time;
                          for (auto &v: _vehicles) {
                            v.next_time += delay;
                          }
                          _vehicles.front().next_time = t;
                        }
                      }
                    }
                    _fully_close_links[index] = false;
                    update_sigma(t);
                  } else if (event.on_port(inputs::IN_CLOSE)) {
                    Data data{};

                    event.data()(data);

                    if (_id_link == 152) {
                      std::cout << std::setprecision(30) << "From LinkQueue : " << this->_id_link << " at " << t << " => CLOSE - " << data.index
                                << " / " << data.open_time << std::endl;
                    }

                    _open_links[data.index] = data.open_time;
                    if (data.open_time == artis::common::DoubleTime::infinity) {
                      _fully_close_links[data.index] = true;
                    }
                  } else if (event.on_port(inputs::BACK)) {
                    Vehicle vehicle;

                    event.data()(vehicle);

//                    if (_id_link == 152 or _id_link == 3) {
//                      std::cout << "From LinkQueue : " << this->_id_link << " BACK at " << t << std::endl;
//                    }

                    vehicle.backward();
                    _vehicles.push_front(Entry{vehicle, t, t});
                    update_sigma(t);
                  }
                }

  );
  _last_time = t;
}

void LinkQueue::start(const artis::traffic::core::Time &t) {
//  std::cout << "From LinkQueue : " << this->_id_link << " start" << std::endl;

  _phase = Phase::WAIT;
  _sigma = artis::common::DoubleTime::infinity;
  _last_time = t;
}

artis::traffic::core::Time LinkQueue::ta(const artis::traffic::core::Time &t) const {
//  std::cout << "From LinkQueue : " << this->_id_link << " ta" << std::endl;
//  std::cout << "Vehicles in queue : " << this->_vehicles.size() << std::endl;
//  if (_vehicles.size() == 1) {
//    std::cout << "time remaining before getting out of queue : " << _vehicles[0].next_time << std::endl;
//    std::cout << "Vehicle enter at  : " << _vehicles[0].in_time << std::endl;
//    std::cout << "Vehicle Id  : " << _vehicles[0].vehicle.index() << std::endl;
//    std::cout << "current time : " << t << std::endl;
//  }

  return _sigma;
}

artis::traffic::core::Bag LinkQueue::lambda(const artis::traffic::core::Time & t) const {
  artis::traffic::core::Bag bag;
//  std::cout << "From LinkQueue : " << this->_id_link << " lambda" << std::endl;

  if (_phase == Phase::SEND) {
    const Vehicle &vehicle = _vehicles.front().vehicle;

//    if (_id_link == 152 or _id_link == 3) {
//      std::cout << "From LinkQueue : " << this->_id_link << " SEND " << t << " " << vehicle.index() << std::endl;
//    }

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
  }
  return bag;
}

artis::common::event::Value LinkQueue::observe(const artis::traffic::core::Time & /* t */,
                                               unsigned int index) const {
  switch (index) {
    case vars::VEHICLE_NUMBER:
      return (unsigned int) _vehicles.size();
    default:
      return {};
  }
}

void LinkQueue::update_state(const artis::traffic::core::Time &t) {
  if (_phase == Phase::WAIT and vehicle_ready(t)) {
    if (not _open_links.empty()) {
      Vehicle &vehicle = _vehicles.front().vehicle;
      unsigned int out_index = vehicle.next_index();

      // if next link is closed for undefined time
      if (_fully_close_links[out_index]) {
        _sigma = artis::common::DoubleTime::infinity;
      } else {
        // if the next link is closed then wait for link to open
        if ((_open_links[out_index] != artis::common::DoubleTime::infinity and t <= _open_links[out_index])) {
          double delay = _open_links[out_index] - t;
          for (auto &v: _vehicles) {
            v.next_time += delay;
          }
          update_sigma(t);
        } else { // else send vehicle to node (and next link)
          _phase = Phase::SEND;
          _vehicles.front().vehicle.data().exit(t);
          if (_vehicles.front().vehicle.path().has_next()) {
            _vehicles.front().vehicle.forward();
          }
          _sigma = 0;
        }
      }
    } else { // send vehicle to node (and next link)
      _phase = Phase::SEND;
      _vehicles.front().vehicle.data().exit(t);
      if (_vehicles.front().vehicle.path().has_next()) {
        _vehicles.front().vehicle.forward();
      }
      _sigma = 0;
    }
  } else if (_phase == Phase::SEND) {
    _phase = Phase::WAIT;
    _vehicles.pop_front();
    update_sigma(t);
  }
}

void LinkQueue::update_sigma(const artis::traffic::core::Time &t) {
  if (_vehicles.empty()) {
    _sigma = artis::common::DoubleTime::infinity;
  } else if (not _fully_close_links.empty()) {
    const Vehicle &vehicle = _vehicles.front().vehicle;

    if (vehicle.path().has_next()) {
      unsigned int out_index = vehicle.next_index();

      if (_fully_close_links[out_index]) {
        _sigma = artis::common::DoubleTime::infinity;
      } else {
        _sigma = _vehicles.front().next_time - t;
      }
    } else {
      _sigma = _vehicles.front().next_time - t;
    }
  } else {
    _sigma = _vehicles.front().next_time - t;
  }
}

bool LinkQueue::vehicle_ready(const artis::traffic::core::Time &t) const {
  return (not _vehicles.empty() and std::abs(_vehicles.front().next_time - t) < 1e-6);
}

}