/**
 * @file artis-traffic/meso/core/LinkTravel.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LinkTravel.hpp"

#include <cmath>
#include <iostream>
#include <iomanip>

namespace artis::traffic::meso::core {

void LinkTravel::dconf(const artis::traffic::core::Time &t,
                       const artis::traffic::core::Time &/* e */,
                       const artis::traffic::core::Bag &bag) {
  dint(t);
  dext(t, 0, bag);
}

void LinkTravel::dint(const artis::traffic::core::Time &t) {
  double e = std::min(_sigma, std::min(_open_sigma, _modify_sigma));

  if (_sigma != artis::common::DoubleTime::infinity) {
    _sigma -= e;
  }
  if (_open_sigma != artis::common::DoubleTime::infinity) {
    _open_sigma -= e;
  }
  if (_modify_sigma != artis::common::DoubleTime::infinity) {
    _modify_sigma -= e;
  }
  update_vehicle_state(t);
  update_sigma(t);
  update_open_close_state(t);
  update_capacity(t);
  _last_time = t;

  if (_id_link == 3) {
    std::cout << std::setprecision(30) << "From LinkTravel : " << this->_id_link << " dint at " << t << " "
              << Phase::to_string(_phase) << " "
              << _sigma << " " << _open_sigma << " " << _modify_sigma << " "
              << OpenClosePhase::to_string(_open_close_phase)
              << " " << (t + ta(t)) << std::endl;
  }

}

void LinkTravel::dext(const artis::traffic::core::Time &t,
                      const artis::traffic::core::Time &e,
                      const artis::traffic::core::Bag &bag) {
  if (_sigma != artis::common::DoubleTime::infinity) {
    _sigma -= e;
  }
  if (_open_sigma != artis::common::DoubleTime::infinity) {
    _open_sigma -= e;
  }
  if (_modify_sigma != artis::common::DoubleTime::infinity) {
    _modify_sigma -= e;
  }
  while (not _delays.empty() and _delays.front().second <= t) {
    _delays.pop_front();
  }

  std::for_each(bag.begin(), bag.end(),
                [t, this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {
                    Vehicle vehicle{};

                    assert((_open_close_phase != OpenClosePhase::CLOSED
                            and _open_close_phase != OpenClosePhase::SEND_CLOSE)
                           or _open_sigma < 1e-6);

                    event.data()(vehicle);

                    assert(_vehicles.empty() or (not _vehicles.empty()
                                                 and t - _vehicles.back().in_time - 1. / _capacity > -1e-6));

                    ++_vehicle_number;
                    ++_total_vehicle_number;
                    ++_moving_vehicle_number;

                    if (_id_link == 3) {
                      std::cout << "From LinkTravel : " << this->_id_link << " IN at " << t << " " << vehicle.index()
                                << std::endl;
                      std::cout << "From LinkTravel : " << this->_id_link << " => " << _vehicles.size() << " / "
                                << _vehicles.front().next_time << std::endl;
                      std::cout << "From LinkTravel : " << this->_id_link << " => "
                                << (_vehicles.front().next_time == t) << std::endl;
                    }

                    double in_time = t;
                    double out_time = in_time + _length / _free_speed;

                    vehicle.data().enter(in_time, _id_link, _total_vehicle_number, out_time);
                    _vehicles.push_back(Entry{vehicle, in_time, out_time});
                    _open_close_phase = OpenClosePhase::SEND_CLOSE;
                    if (_vehicle_number >= (unsigned int) (_concentration * _length)) {
                      _open_sigma = artis::common::DoubleTime::infinity;
                    } else {
                      _open_sigma = 1. / _capacity;
                    }
                    _sigma = 0;
                  } else if (event.on_port(inputs::IN_CONFIRM_OUT)) {
                    Vehicle vehicle{};

                    event.data()(vehicle);
                    --_vehicle_number;

                    // stat
                    _vehicle_numbers.emplace_back(vehicle.data().t_entry + (_length / 2) / (_length /
                                                                                            (vehicle.data().t_real_end -
                                                                                             vehicle.data().t_entry)));
                    while (not _vehicle_numbers.empty() and _vehicle_numbers.back() - _vehicle_numbers.front() > 60) {
                      _vehicle_numbers.pop_front();
                    }
                    _vehicle_durations.emplace_back(t, vehicle.data().t_real_end - vehicle.data().t_entry);
                    while (not _vehicle_durations.empty() and
                           _vehicle_durations.back().first - _vehicle_durations.front().first > 60) {
                      _vehicle_durations.pop_front();
                    }

                    if (_vehicle_number <= (unsigned int) (_concentration * _length) and
                        _open_close_phase == OpenClosePhase::CLOSED and
                        _open_sigma == artis::common::DoubleTime::infinity) {
                      _open_sigma = std::max(0., 1. / _capacity - (t - _vehicles.back().in_time));
                      update_open_close_state(t);
                    }
                  }
                });
  _last_time = t;
}

void LinkTravel::start(const artis::traffic::core::Time &t) {
  _vehicle_number = 0;
  _total_vehicle_number = 0;
  _moving_vehicle_number = 0;
  _open_sigma = 0;
  _open_close_phase = OpenClosePhase::SEND_OPEN;
  _phase = Phase::WAIT;
  _sigma = artis::common::DoubleTime::infinity;
  _modify_sigma = _modified_capacities.empty() ? artis::common::DoubleTime::infinity :
                  _modified_capacities.front().first - t;
  _last_time = t;
}

artis::traffic::core::Time LinkTravel::ta(const artis::traffic::core::Time & /* t */) const {
  return std::min(_sigma, std::min(_open_sigma, _modify_sigma));
}

artis::traffic::core::Bag LinkTravel::lambda(const artis::traffic::core::Time &t) const {
  artis::traffic::core::Bag bag;

  if (_phase == Phase::SEND) {
    const Vehicle &vehicle = _vehicles.front().vehicle;

    if (_id_link == 3) {
      std::cout << "From LinkTravel : " << this->_id_link << " SEND at " << t << " " << vehicle.index() << std::endl;
    }

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
  }
  if (_open_close_phase == OpenClosePhase::SEND_OPEN) {

    if (_id_link == 3) {
      std::cout << "From LinkTravel : " << this->_id_link << " SEND OPEN at " << t << std::endl;
    }

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_OPEN));
  } else if (_open_close_phase == OpenClosePhase::SEND_CLOSE) {

    if (_id_link == 3) {
      std::cout << std::setprecision(30) << "From LinkTravel : " << this->_id_link << " SEND CLOSE at " << t << " / "
                << (t + _open_sigma)
                << " / " << _open_sigma << std::endl;
    }

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE, (double) (t + _open_sigma)));
  }
  return bag;
}

artis::common::event::Value LinkTravel::observe(const artis::traffic::core::Time & /* t */,
                                                unsigned int index) const {
  switch (index) {
    case vars::VEHICLE_NUMBER:
      return (unsigned int) _vehicles.size();
    case vars::TOTAL_VEHICLE_NUMBER:
      return (unsigned int) _total_vehicle_number;
    case vars::FLOW:
      return double(_vehicle_numbers.size()) / 60.;
    case vars::SPEED_AVERAGE: {
      double sum = 0;

      if (_vehicle_durations.empty()) {
        return (double) 0.;
      } else {
        std::for_each(_vehicle_durations.cbegin(), _vehicle_durations.cend(),
                      [&sum](const auto &p) { sum += p.second; });
        sum /= double(_vehicle_durations.size());
        return _length / sum;
      }
    }
    case vars::MOVING_VEHICLE_NUMBER:
      return (unsigned int) _moving_vehicle_number;
    default:
      return {};
  }
}

void LinkTravel::update_vehicle_state(const artis::traffic::core::Time &t) {
  if (_phase == Phase::WAIT and vehicle_ready(t)) {
    _phase = Phase::SEND;
    _sigma = 0;
  } else if (_phase == Phase::SEND) {
    _phase = Phase::WAIT;
    _vehicles.pop_front();
    --_moving_vehicle_number;
  }
}

void LinkTravel::update_open_close_state(const artis::traffic::core::Time & /* t */) {
  if (_open_close_phase == OpenClosePhase::SEND_OPEN) {
    _open_close_phase = OpenClosePhase::OPENED;
    _open_sigma = artis::common::DoubleTime::infinity;
  } else if (_open_close_phase == OpenClosePhase::SEND_CLOSE) {
    if (_open_sigma > 0) {
      _open_close_phase = OpenClosePhase::CLOSED;
    } else {
      _open_close_phase = OpenClosePhase::OPENED;
      _open_sigma = artis::common::DoubleTime::infinity;
    }
  } else if (_open_close_phase == OpenClosePhase::CLOSED and _open_sigma < 1e-6) {
    _open_close_phase = OpenClosePhase::SEND_OPEN;
    _open_sigma = 0;
  }
}

void LinkTravel::update_sigma(const artis::traffic::core::Time &t) {
  if (_vehicles.empty()) {
    _sigma = artis::common::DoubleTime::infinity;
  } else {
    _sigma = _vehicles.front().next_time - t;
  }
}

void LinkTravel::update_capacity(const artis::traffic::core::Time &t) {
  if (_modify_sigma < 1e-6) {
    _capacity = _modified_capacities.front().second;
    _modified_capacities.pop_front();
    if (not _modified_capacities.empty()) {
      _modify_sigma = _modified_capacities.front().first - t;
    } else {
      _modify_sigma = artis::common::DoubleTime::infinity;
    }
    if (_vehicles.back().in_time + 1. / _capacity > t) {
      _open_close_phase = OpenClosePhase::SEND_CLOSE;
      _open_sigma = _vehicles.back().in_time + 1. / _capacity - t;
      _sigma = 0;
    }
  }
}

bool LinkTravel::vehicle_ready(const artis::traffic::core::Time &t) const {
  return (not _vehicles.empty() and std::abs(_vehicles.front().next_time - t) < 1e-6);
}

}